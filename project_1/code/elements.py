#!/usr/bin/env python
"""
AUTHOR : Rohit Tripathy

A script to define classes for each type of finite element

Elements defined :  1. 8 node serendipity quad element
                    2. 6 node triangular element

"""
import numpy as np
import math
import os
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm

__all__ = ['serendipity_8', 'triangle_6']

class serendipity_8:
    def __init__(self, points = None,
                 node_numbers = None,
                 k = None,
                 s = None,
                 gamma_t = None,
                 gamma_q = None,
                 T = None,
                 q = None):
        """
        Initialize the object
        
        ::Param points:: Element global nodal coordinates(in order)
        ::Param k:: Element conductivity
        ::Param s:: Element source term
        ::Param gamma_t:: coordinates on which Dirichlet BC will be applied
        ::Param gamma_q:: coordinates on which Neumann BC will be applied
        """
        #Global node numbers
        self.node_numbers = node_numbers
        
        #Temperature at each node
        self.temp = np.zeros(8)
        
        #Prescribed temperature at Dirichlet Boundary  
        self.T = T
        
        #Prescribed flux at Neumann Boundary
        self.q = q
        
        #define natural coordinate system
        self.natural_coordinates = [[-1., -1.], [1., -1], [1., 1.], [-1., 1.], \
            [0, -1.], [1., 0.], [0., 1.], [-1., 0.]]
        
        
        #Points on which Neumann bc is imposed
        self.gamma_q = gamma_q
        
        #Points on which Dirichlet BC is imposed
        self.gamma_T = gamma_t
        
        #Source term if any 
        self.s = s
        
        #Element conductivity
        self.k = k
        
        #x coordinates of all points 
        self.x = points[:, 0]

        #y coordinates of all points 
        self.y = points[:, 1]
        
        #Element nodal coordinates matrix 
        self.Pe = points
        
        #Flux at all points
        self.Q = np.zeros((8, 2))
        
        #Element conductivity matrix 
        self.Ke = np.zeros(shape = (8, 8))
        self.K_max = 0
        
        #Element forcing term in the domain 
        self.fel_omega = np.zeros(8)
        
        #Element boundary forcing term 
        self.fel_gamma = np.zeros(8)
        
        #Store the quadrature points 
        a = 1. / np.sqrt(3)
        self.quad_points = [(-a, -a), (a, -a), (a, a), (-a, a)]
        
        #generate stiffness matrix
        self._stiffness_matrix()
    
        #generate fel_omega
        self._compute_fel_omega()
        
        #Impose dirichlet BC
        if len(self.gamma_T):
            self._dirichlet_bc()
        
        
    def shape_functions_matrix(self, psi, eta):
        """
        Routine to compute shape function matrix for given psi and eta.
        
        ::Param psi:: psi coordinate
        ::Param eta:: eta coordinate
        """
        N1 = (-1. / 4) * (1 - psi) * (1 - eta) * (1 +psi + eta)
        N2 = (-1. / 4) * (1 + psi) * (1 - eta) * (1 -psi + eta)
        N3 = (-1. / 4) * (1 + psi) * (1 + eta) * (1 -psi - eta)
        N4 = (-1. / 4) * (1 - psi) * (1 + eta) * (1 +psi - eta)   
        N5 = (1. / 2) * (1 - psi) * (1 + psi) * (1 - eta)
        N6 = (1. / 2) * (1 + psi) * (1 - eta) * (1 + eta)
        N7 = (1. / 2) * (1 - psi) * (1 + psi) * (1 + eta)
        N8 = (1. / 2) * (1 - psi) * (1 - eta) * (1 + eta)
        N = [N1, N2, N3, N4, N5, N6, N7, N8]
        return np.asarray(N)
    
    def gradient_matrix(self, psi, eta):
        """
        Routine to generate the gradient matrix for given psi and eta
        ::Param psi:: psi coordinate
        ::Param eta:: eta coordinate
        """
        #gradients with respect to psi
        N1_psi = ((1. / 4) * (1 - eta) * (1. + psi + eta)) - ((1. / 4) * (1. - psi) * (1. - eta))
        N2_psi = ((-1. / 4) * (1 - eta) * (1. - psi + eta)) + ((1. / 4) * (1. + psi) * (1. - eta))
        N3_psi = ((-1. / 4) * (1 + eta) * (1. - psi - eta)) + ((1. / 4) * (1. + eta) * (psi + 1.))
        N4_psi = ((1. / 4) * (1 + eta) * (1. + psi - eta)) - ((1. / 4) * (1. + eta) * (-psi + 1.))
        N5_psi = ((1. / 2) * (1 - psi) * (1 - eta)) - ((1. / 2) * (1 + psi) * (1 - eta))
        N6_psi = (1. / 2) * (1 + eta) * (1 - eta)
        N7_psi = ((1. / 2) * (1 - psi) * (1 + eta)) - ((1. / 2) * (1 + psi) * (1 + eta))
        N8_psi = -(1. / 2) * (1 + eta) * (1 - eta)
        
        #gradient wrt eta
        N1_eta = -((1. / 4) * (1 - psi) * (1 - eta)) + ((1. / 4) * (1 - psi) * (1 + psi + eta))
        N2_eta = ((1. / 4) * (1 + psi) * (1 - psi + eta)) - ((1. / 4) * (1 + psi) * (1 - eta)) 
        N3_eta = -((1. / 4) * (1 + psi) * (1. - psi - eta)) + ((1. / 4) * (1. + eta) * (1 + psi))
        N4_eta = -((1. / 4) * (1 - psi) * (1. + psi - eta)) + ((1. / 4) * (1. + eta) * (1 - psi))
        N5_eta = -(1. / 2) * (1 - psi) * (1 + psi)
        N6_eta = -((1. / 2) * (1 + psi) * (1 + eta)) + ((1. / 2) * (1 + psi) * (1 - eta))
        N7_eta = (1. / 2) * (1 - psi) * (1 + psi) 
        N8_eta = -((1. / 2) * (1 - psi) * (1 + eta)) + ((1. / 2) * (1 - psi) * (1 - eta))
        
        #assemble into elemental gradient matrix
        Ge = np.array([[N1_psi, N2_psi, N3_psi, N4_psi, N5_psi, N6_psi, N7_psi, N8_psi],
            [N1_eta, N2_eta, N3_eta, N4_eta, N5_eta, N6_eta, N7_eta, N8_eta]])

        #print 'gradient'
        #return Ge
        return Ge
    
    def jacobian(self, G, points):
        """
        Routine to compute and return the Jacobian matrix.
        
        ::Param G:: Gradient matrix
        ::Param points:: Points matrix
        """
        #print 'jacobian'
        Je = np.dot(G, points)
        return Je
    
    def B_matrix(self, jacobian = None, gradient = None):
        """
        Routine to compute B matrix
        
        ::Param jacobian:: Jacobian matrix
        ::Param gradient:: Gradient matrix
        """
        #print 'B'
        jacobian_inverse = np.linalg.inv(jacobian)
        Be = np.dot(jacobian_inverse, gradient)
        return Be
    
    def _stiffness_matrix(self):
        for i, quad_point in enumerate(self.quad_points):
            psi = quad_point[0]
            eta = quad_point[1]
            G = self.gradient_matrix(psi, eta)
            J = self.jacobian(G, self.Pe)
            B = self.B_matrix(J, G)
            self.Ke += self.k * np.linalg.det(J) * np.dot(B.T, B)
        self.K_max = np.max(self.Ke)

    def _compute_fel_omega(self):
        for i in xrange(4):
            psi = self.quad_points[i][0]
            eta = self.quad_points[i][1]
            G = self.gradient_matrix(psi, eta)
            J = self.jacobian(G, self.Pe)
            N = self.shape_functions_matrix(psi, eta)
            s = np.dot(N, self.s)
            self.fel_omega += s * np.linalg.det(J) * N
            
    def _dirichlet_bc(self):
        #loop over all nodes having dirichlet BC
        for i, point in enumerate(self.Pe):
            for node in self.gamma_T:
                if np.array_equal(point, node):
                    C = (10 ** 5) * self.K_max
                    self.Ke[i, i] = C
                    self.fel_omega[i] = C * self.T[i]
    
    def _natural_coordinate(self, point):
        """
        Take coordinate in global CS. Return its corresponding
        natural Coordinate
        """
        for i, c in enumerate(self.Pe):
            if np.array_equal(c, point):
                return self.natural_coordinates[i]
            
    def _neumann_bc(self,
                    gamma_q = None,
                    q = None,
                    bc_on_psi = None,
                    bc_on_eta = None):
        
        if bc_on_psi:
            psi = gamma_q[0][0]

        if bc_on_eta:
            eta = gamma_q[0][1]
        
        #evaluate J_gamma_det
        for i, qp in enumerate([(1. / np.sqrt(3)), -(1. / np.sqrt(3))]):
            if bc_on_psi:
                eta = qp
                G = self.gradient_matrix(psi, eta)
                dx_deta = np.sum(G[1, :] * self.x)
                dy_deta = np.sum(G[1, :] * self.y)
                J_gamma_det = np.sqrt(dx_deta ** 2 + dy_deta ** 2)
                
            if bc_on_eta:
                psi = qp
                G = self.gradient_matrix(psi, eta)
                dx_dpsi = np.sum(G[0, :] * self.x)
                dy_dpsi = np.sum(G[0, :] * self.y)
                J_gamma_det = np.sqrt(dx_dpsi ** 2 + dy_dpsi ** 2)
                
            N = self.shape_functions_matrix(psi, eta)
            
            q_bar = np.dot(N, q)
            self.fel_gamma -= q_bar * J_gamma_det * N
    
    
    def compute_flux(self):
        for i, node in enumerate(self.node_numbers):
            psi = self.natural_coordinates[i][0]
            eta = self.natural_coordinates[i][1]
            G = self.gradient_matrix(psi, eta)
            J = self.jacobian(G, self.Pe)
            B = self.B_matrix(J, G)
            q = -self.k * np.dot(B, self.temp)
            self.Q[i] = q
    
    
class triangle_6:
    def __init__(self, points = None,
                 node_numbers = None,
                 k = None,
                 s = None,
                 gamma_t = None,
                 gamma_q = None,
                 T = None,
                 q = None):
        """
        Initialize the object
        
        ::Param points:: Element global nodal coordinates(in order)
        ::Param k:: Element conductivity
        ::Param s:: Element source term
        ::Param gamma_t:: coordinates on which Dirichlet BC will be applied
        ::Param gamma_q:: coordinates on which Neumann BC will be applied
        """
        #Global node numbers
        self.node_numbers = node_numbers
        
        #Temperature at each node
        self.temp = np.zeros(6)
        
        #Prescribed temperature at Dirichlet Boundary  
        self.T = T
        
        #Prescribed flux at Neumann Boundary
        self.q = q
        
        #define natural coordinate system
        self.natural_coordinates = [[0., 0.], [1., 0], [0., 1.], [0.5, 0.], \
            [0.5, 0.5], [0., 0.5]]
        
        
        #Points on which Neumann bc is imposed
        self.gamma_q = gamma_q
        
        #Points on which Dirichlet BC is imposed
        self.gamma_T = gamma_t
        
        #Source term if any 
        self.s = s
        
        #Element conductivity
        self.k = k
        
        #x coordinates of all points 
        self.x = points[:, 0]

        #y coordinates of all points 
        self.y = points[:, 1]
        
        #Element nodal coordinates matrix 
        self.Pe = points
        
        #Flux at all points
        self.Q = np.zeros((6, 2))
        
        #Element conductivity matrix 
        self.Ke = np.zeros(shape = (6, 6))
        self.K_max = 0
        
        #Element forcing term in the domain 
        self.fel_omega = np.zeros(6)
        
        #Element boundary forcing term 
        self.fel_gamma = np.zeros(6)
        
        #Store the quadrature points and weights
        self.quad_points = [(1. / 3, 1. / 3), (0.2, 0.6), (0.2, 0.2), (0.6, 0.2)]
        self.weights = [-(27. / 48), (25. / 48), (25. / 48), (25. / 48)]
        
        #generate stiffness matrix
        self._stiffness_matrix()
    
        #generate fel_omega
        self._compute_fel_omega()
        
        #Impose dirichlet BC
        if len(self.gamma_T):
            self._dirichlet_bc()
        
        
    def shape_functions_matrix(self, psi, eta):
        """
        Routine to compute shape function matrix for given psi and eta.
        
        ::Param psi:: psi coordinate
        ::Param eta:: eta coordinate
        """
        N1 = 2. * (1 - psi - eta) * (0.5 - psi -eta)
        N2 = 2. * psi * (psi - 0.5)
        N3 = 2. * eta * (eta - 0.5)
        N4 = 4. * psi * (1. - psi - eta)  
        N5 = 4. * psi * eta
        N6 = 4. * eta * (1. - psi - eta) 
        N = [N1, N2, N3, N4, N5, N6]
        return np.asarray(N)
    
    def gradient_matrix(self, psi, eta):
        """
        Routine to generate the gradient matrix for given psi and eta
        ::Param psi:: psi coordinate
        ::Param eta:: eta coordinate
        """
        #gradients with respect to psi
        N1_psi = (-2. * (1 - psi - eta)) +(-2. * (0.5 - psi -eta))
        N2_psi = (2 * psi) + (2 * (psi - 0.5))
        N3_psi = 0.
        N4_psi = (-4 * psi) + (4. * (1. - psi - eta))
        N5_psi = 4 * eta
        N6_psi = -4 * eta
        
        #gradient wrt eta
        N1_eta = (-2. * (1 - psi - eta)) + (-2. * (0.5 - psi -eta))
        N2_eta = 0. 
        N3_eta = (2 * eta)  + ( 2 * (eta - 0.5))
        N4_eta = -4 * psi
        N5_eta = 4 * psi
        N6_eta = (-4 * eta) + (4. * (1. - psi - eta))
        
        #assemble into elemental gradient matrix
        Ge = np.array([[N1_psi, N2_psi, N3_psi, N4_psi, N5_psi, N6_psi],
            [N1_eta, N2_eta, N3_eta, N4_eta, N5_eta, N6_eta]])

        #print 'gradient'
        #return Ge
        return Ge
    
    def jacobian(self, G, points):
        """
        Routine to compute and return the Jacobian matrix.
        
        ::Param G:: Gradient matrix
        ::Param points:: Points matrix
        """
        #print 'jacobian'
        Je = np.dot(G, points)
        return Je
    
    def B_matrix(self, jacobian = None, gradient = None):
        """
        Routine to compute B matrix
        
        ::Param jacobian:: Jacobian matrix
        ::Param gradient:: Gradient matrix
        """
        #print 'B'
        jacobian_inverse = np.linalg.inv(jacobian)
        Be = np.dot(jacobian_inverse, gradient)
        return Be
    
    def _stiffness_matrix(self):
        for i, quad_point in enumerate(self.quad_points):
            psi = quad_point[0]
            eta = quad_point[1]
            G = self.gradient_matrix(psi, eta)
            J = self.jacobian(G, self.Pe)
            B = self.B_matrix(J, G)
            self.Ke += self.k * self.weights[i] * np.linalg.det(J) * np.dot(B.T, B)
        self.K_max = np.max(self.Ke)

    def _compute_fel_omega(self):
        for i in xrange(4):
            psi = self.quad_points[i][0]
            eta = self.quad_points[i][1]
            G = self.gradient_matrix(psi, eta)
            J = self.jacobian(G, self.Pe)
            N = self.shape_functions_matrix(psi, eta)
            s = np.dot(N, self.s)
            self.fel_omega += s * np.linalg.det(J) * N
            
    def _dirichlet_bc(self):
        #loop over all nodes having dirichlet BC
        for i, point in enumerate(self.Pe):
            for node in self.gamma_T:
                if np.array_equal(point, node):
                    C = (10 ** 5) * self.K_max
                    self.Ke[i, i] = C
                    self.fel_omega[i] = C * self.T[i]
                    
    def _natural_coordinate(self, point):
        """
        Take coordinate in global CS. Return its corresponding
        natural Coordinate
        """
        for i, c in enumerate(self.Pe):
            if np.array_equal(c, point):
                return self.natural_coordinates[i]
            
    def _neumann_bc(self,
                    gamma_q = None,
                    q = None,
                    bc_on_psi = None,
                    bc_on_eta = None):

        if bc_on_psi:
            psi = gamma_q[0][0]

        if bc_on_eta:
            eta = gamma_q[0][1]
        
        #evaluate J_gamma_det
        for i, qp in enumerate([(1. / np.sqrt(3)), -(1. / np.sqrt(3))]):
            if bc_on_psi:
                eta = (1. + qp) / 2.
                G = self.gradient_matrix(psi, eta)
                dx_deta = np.sum(G[1, :] * self.x)
                dy_deta = np.sum(G[1, :] * self.y)
                J_gamma_det = np.sqrt(dx_deta ** 2 + dy_deta ** 2)
                
            if bc_on_eta:
                psi = (1. + qp) / 2.
                G = self.gradient_matrix(psi, eta)
                dx_dpsi = np.sum(G[0, :] * self.x)
                dy_dpsi = np.sum(G[0, :] * self.y)
                J_gamma_det = np.sqrt(dx_dpsi ** 2 + dy_dpsi ** 2)
                
            N = self.shape_functions_matrix(psi, eta)
            
            q_bar = np.dot(N, q)
            self.fel_gamma -= q_bar * J_gamma_det * N

    
    def compute_flux(self):
        for i, node in enumerate(self.node_numbers):
            psi = self.natural_coordinates[i][0]
            eta = self.natural_coordinates[i][1]
            G = self.gradient_matrix(psi, eta)
            J = self.jacobian(G, self.Pe)
            B = self.B_matrix(J, G)
            q = -self.k * np.dot(B, self.temp)
            self.Q[i] = q
            
            
            