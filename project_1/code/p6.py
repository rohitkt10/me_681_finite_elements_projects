#!/usr/bin/env python

import numpy as np
import math
import os
import sys
sys.path.insert(0, '../starter_files/')
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from elements import *
from scipy.io import loadmat
import cPickle as pickle
import scipy.sparse as scysparse
from scipy.interpolate import griddata

################################
#create folder for storing plots
################################
figure_folder = "../figures/problem3_bonus/"
if not os.path.exists(figure_folder):
    os.makedirs(figure_folder)

#folder to store pickled data
data_folder = '../data/'
if not os.path.exists(data_folder):
    os.makedirs(data_folder)
    
#folder where .mat files are stored
mat_folder = '../mat_files/'


##########
#Problem 3
##########
def _T_analytical(x):
    return (np.sqrt((x ** 2) + (y ** 2)) - 0.25) ** 2

#source term
def _s(x, y):
    return ((2 * 0.25) / (np.sqrt((x ** 2) + (y ** 2)))) - 4
    
#Neumann BC on the top
def _q_top(x, y):
    return  1. * ((0.25 / np.sqrt((x ** 2) + (y ** 2))) - 1.)

#Neumann BC on the bottom
def _q_bottom(x, y):
    return  1. * (- (0.25 / np.sqrt((x ** 2) + (y ** 2))) + 1.)

#Dirchlet BCs
def _T_sides(x, y):
    return (0.25 * 0.25) + (0.5 * 0.5) + (y * y) - 0.5 * np.sqrt((x ** 2) + (y ** 2))

#Thermal conductivity
k = 1.0

#Store max absolute error in this list
max_abs_error = []

#Store RMS error in this list
rms_error = []

filenames = ['rect_plate_with_hole_1_tri', 'rect_plate_with_hole_2_tri', 'rect_plate_with_hole_3_tri']

for filename in filenames:
    #Check for pickled file
    if not os.path.exists(data_folder + filename + '.pkl'):
        #Get mesh information
        data = loadmat(mat_folder + filename + '.mat')
        
        #Dump the optimized model object into a pickle file
        mesh_data = open(data_folder + filename + '.pkl', 'wb')
        
        pickle.dump(data, mesh_data)
        mesh_data.close()
    
    else:
        input_stream = open(data_folder + filename + '.pkl', 'rb')
        data = pickle.load(input_stream)
        input_stream.close()
    
    node_coordinates = data['nodes']
    element_data = data['elements']
    N = len(node_coordinates)
    
    x = node_coordinates[:, 1]
    y = node_coordinates[:, 2]

    
    #get number of elements 
    nel = element_data.shape[0]
    
    #define global matrices
    K_global = np.zeros(shape =(N, N))
    f_global = np.zeros(N)
    
    #Define the list of elements
    elements = []
    for i in xrange(nel):
        points = []
        node_numbers = element_data[i, 1:]
        node_numbers[0], node_numbers[1] = node_numbers[1], node_numbers[0]
        node_numbers[1], node_numbers[2] = node_numbers[2], node_numbers[1]
        node_numbers[4], node_numbers[3] = node_numbers[3], node_numbers[4]
        node_numbers[5], node_numbers[4] = node_numbers[4], node_numbers[5]
        
        #get points matrix
        for j, node in enumerate(node_numbers):
            points.append(node_coordinates[node - 1, 1:])
        points = np.asarray(points)
        
        #define source
        x = points[:, 0]
        y = points[:, 1]
        s = _s(x, y)
   
        #get Dirichlet Boundary:
        gamma_t = []
        T = np.zeros(6)
        for i, point in enumerate(points):
            x = point[0]
            y = point[1]
            if point[0] == -0.5 or point[0] == 0.5:
                gamma_t.append(point)
                T[i] = _T_sides(x, y)
            
            if x ** 2 + y ** 2 == 0.25 * 0.25:
                gamma_t.append(point)
                T[i] = _T_sides(x, y)
        
        #typecast into numpy matrix for ease of working
        gamma_t = np.asarray(gamma_t)
        
        #Neumann boundary condition
        gamma_q = []
        q = np.zeros(6)
        for i, point in enumerate(points):
            x = point[0]
            y = point[1]
            if y == 0.5:
                gamma_q.append(point)
                q[i] = _q_top(x, y)
            if y == -0.5:
                gamma_q.append(point)
                q[i] = _q_bottom(x, y)
        
        #Now define the element
        element = triangle_6(points = points,
                                node_numbers = node_numbers,
                                k = k,
                                s = s,
                                gamma_t = gamma_t,
                                gamma_q = gamma_q,
                                T = T,
                                q = q)
        
        if element.gamma_q != None:
            if len(element.gamma_q):
                element._neumann_bc(element.gamma_q, q = q, bc_on_eta = True)
        
        K_local = element.Ke
        
        #Assemble global stiffness matrix
        index1, index2 = 0, 0
        for i in node_numbers:
            for j in node_numbers:
                K_global[i - 1, j - 1] += K_local[index1, index2]
                index2 += 1
            index2 = 0
            index1 += 1
        
        fel_gamma = element.fel_gamma
        fel_omega = element.fel_omega
        f_local = fel_gamma + fel_omega
        
        #Assemble global forcing vector
        for i, j in enumerate(node_numbers):
            f_global[j - 1] += f_local[i]
        
        elements.append(element)
    
    T = np.linalg.solve(K_global, f_global)
    
    Q_global = np.zeros((N, 2))
    
    #Compute global flux
    temp = np.zeros(6)
    for element in elements:
        for i, j in enumerate(element.node_numbers):
            temp[i] = T[j - 1]
        element.temp = temp
        element.compute_flux()
        for k, node in enumerate(element.node_numbers):
            Q_global[node - 1] = np.asarray(element.Q[k])
    

    x = node_coordinates[:, 1]
    y = node_coordinates[:, 2]
    
    #Quiver Plot of Flux
    u = Q_global[:, 0]
    v = Q_global[:, 1]
    plt.quiver(x, y, u, v, color='r', units='x', linewidths=(2,), edgecolors=('k'), headaxislength=5 )
    plt.xlabel('$x$', fontsize = 15)
    plt.ylabel('$y$', fontsize = 15)
    plt.title("Quiver Plot of Flux $q$; $N=$" + str(N))
    plt.savefig(figure_folder + 'q3_bonus_Rohit_quiver_plot_N_' + str(N) + '.pdf')
    plt.close()
    
    x = node_coordinates[:, 1]
    y = node_coordinates[:, 2]
    x_max, x_min = np.max(x), np.min(x)
    y_max, y_min = np.max(x), np.min(x)
    x_mean = (x_max + x_min) / 2.
    y_mean = (y_max + y_min) / 2.
    
    #Error plot
    T_analytical = _T_analytical(x)
    error = np.log(np.abs(T_analytical - T))
    max_abs_error.append(np.max(np.abs(T_analytical - T)))
    rms_error.append(np.sqrt(np.mean((T_analytical - T) ** 2)))
    index = np.arange(1, N + 1)
    plt.plot(index, error, 'r-', linewidth = 2.0, marker = '^')
    plt.title('Absolute Error; $N=$' + str(N))
    plt.xlabel('Node number')
    plt.ylabel('$log(|T_{analytical} - T_{numerical}|)$')
    plt.savefig(figure_folder + 'q3_bonus_Rohit_error_plot_N_' + str(N) + '.pdf')
    plt.close()
    
    
    #contour plot of Temperature 
    xi = np.linspace(-0.5, 0.5, 100)
    yi = np.linspace(-0.5, 0.5, 100)
    zi = griddata((x, y), T, (xi[None, :], yi[:, None]), fill_value = 0., method = 'cubic')
    for i in xrange(100):
        for j in xrange(100):
            if xi[i] ** 2 + yi[j] ** 2 <= 0.25 * 0.25:
                zi[i, j] = 0.
    CS = plt.contour(xi,yi,zi,15,linewidths=0.5,colors='k')
    CS = plt.contourf(xi,yi,zi,15,cmap=plt.cm.jet)
    circ = plt.Circle((x_mean, y_mean), 0.25, color='w')
    ax=plt.gca()
    ax.add_patch(circ)
    plt.colorbar()
    plt.xlabel('$x$', fontsize = 15)
    plt.ylabel('$y$', fontsize = 15)
    plt.title('Contour plot of $T(x, y)$; $N=$' + str(N))
    plt.savefig(figure_folder + 'q3_bonus_Rohit_temp_contour_N_' + str(N) + '.pdf')
    plt.close()

#define an index vector
index = np.arange(1, 4)

#plot maximum abs. error
plt.plot(index, max_abs_error, 'r^-', linewidth = 2.5, label = 'max(|$T_{analytical} - T_{numerical}$|)')
plt.plot(index, rms_error, 'bo-', linewidth = 2.5, label = 'RMSE')
plt.legend(loc = 'best')
plt.xlabel('Number of nodes')
plt.ylabel('Error')
plt.title('Error vs level of discretization')
plt.savefig(figure_folder + 'q3_bonus_Rohit_error_vs_disretization.pdf')
plt.close()
    
    
    