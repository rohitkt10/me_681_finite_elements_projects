#!/usr/bin/env python

print 'Executing Problem 1 .....'
execfile('p1.py')
print 'Executing Problem 2 .....'
execfile('p2.py')
print 'Executing Problem 3 .....'
execfile('p3.py')
print 'Now executing Bonus problems'
execfile('p4.py')
execfile('p5.py')
execfile('p6.py')
print 'All problems have been successfully executed and all plots have been generated'
print 'Please check the path : \'./../figures/\''