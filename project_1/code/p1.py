#!/usr/bin/env python

import numpy as np
import math
import os
import sys
sys.path.insert(0, '../starter_files/')
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from elements import *
from scipy.io import loadmat
import cPickle as pickle
from scipy.interpolate import griddata

################################
#create folder for storing plots
################################
figure_folder = "../figures/"
if not os.path.exists(figure_folder):
    os.makedirs(figure_folder)
    
#folder to store pickled data
data_folder = '../data/'
if not os.path.exists(data_folder):
    os.makedirs(data_folder)

##########
#Problem 1
##########

def _T_analytical(x):
    return x + 0.5

figure_folder = '../figures/problem1/'
if not os.path.exists(figure_folder):
    os.makedirs(figure_folder)
    
#folder where .mat files are stored
mat_folder = '../mat_files/'

#source term
s = 0

#Thermal conductivity
k = 1.0


#Check for pickled file
if not os.path.exists(data_folder + 'rect_plate_wo_hole_q2_4.pkl'):
    #Get mesh information
    data = loadmat(mat_folder + 'rect_plate_wo_hole_q2_4.mat')
    
    #Dump the optimized model object into a pickle file
    mesh_data = open(data_folder + 'rect_plate_wo_hole_q2_4.pkl', 'wb')
    
    pickle.dump(data, mesh_data)
    mesh_data.close()

else:
    input_stream = open(data_folder + 'rect_plate_wo_hole_q2_4.pkl', 'rb')
    data = pickle.load(input_stream)
    input_stream.close()

node_coordinates = data['nodes']
element_data = data['elements']

N = len(node_coordinates)

x = node_coordinates[:, 1]
y = node_coordinates[:, 2]

#get number of elements 
nel = element_data.shape[0]

K_global = np.zeros(shape =(N, N))
f_global = np.zeros(N)

#Define the list of elements
elements = []

for i in xrange(nel):
    points = []
    node_numbers = element_data[i, 1:]
    node_numbers[0], node_numbers[2] = node_numbers[2], node_numbers[0]
    node_numbers[1], node_numbers[3] = node_numbers[3], node_numbers[1]
    node_numbers[4], node_numbers[6] = node_numbers[6], node_numbers[4]
    node_numbers[5], node_numbers[7] = node_numbers[7], node_numbers[5]
    
    #get points matrix
    for j, node in enumerate(node_numbers):
        points.append(node_coordinates[node - 1, 1:])
    points = np.asarray(points)

    
    #get Dirichlet Boundary:
    gamma_t = []
    T = np.zeros(8)
    for i, point in enumerate(points):
        if point[0] == 0.5:
            gamma_t.append(point)
            T[i] = 1.
        if point[0] == -0.5:
            gamma_t.append(point)
            T[i] = 0
    
    #typecast into numpy matrix for ease of working
    gamma_t = np.asarray(gamma_t)
    

    #Now define the element
    element = serendipity_8(points = points,
                            node_numbers = node_numbers,
                            k = k,
                            s = s,
                            gamma_t = gamma_t,
                            T = T)
    

    K_local = element.Ke
    
    #Assemble global stiffness matrix
    index1, index2 = 0, 0
    for i in node_numbers:
        for j in node_numbers:
            K_global[i - 1, j - 1] += K_local[index1, index2]
            index2 += 1
        index2 = 0
        index1 += 1
    
    fel_gamma = element.fel_gamma
    fel_omega = element.fel_omega
    f_local = fel_gamma + fel_omega
    
    #Assemble global forcing vector
    for i, j in enumerate(node_numbers):
        f_global[j - 1] += f_local[i]
        
    elements.append(element)
    
#Global Temperature Vector
T = np.linalg.solve(K_global, f_global)

Q_global = np.zeros((N, 2))

#Compute global flux
temp = np.zeros(8)
for element in elements:
    for i, j in enumerate(element.node_numbers):
        temp[i] = T[j - 1]
    element.temp = temp
    element.compute_flux()
    for k, node in enumerate(element.node_numbers):
        Q_global[node - 1] = np.asarray(element.Q[k])


#Quiver Plot of Flux
u = Q_global[:, 0]
v = Q_global[:, 1]
plt.quiver(x, y, u, v, color='r', units='x', linewidths=(2,), edgecolors=('k'), headaxislength=5 )
plt.xlabel('$x$', fontsize = 15)
plt.ylabel('$y$', fontsize = 15)
plt.title("Quiver Plot of Flux $q$")
plt.savefig(figure_folder + 'q1_Rohit_quiver_plot.pdf')
plt.close()

#contour plot of Temperature 
xi = np.linspace(-0.5, 0.5, 5)
yi = np.linspace(-0.5, 0.5, 5)
zi = griddata((x, y), T, (xi[None, :], yi[:, None]), method = 'cubic')
CS = plt.contour(xi,yi,zi,15,linewidths=0.5,colors='k')
CS = plt.contourf(xi,yi,zi,15,cmap=plt.cm.jet)
plt.colorbar()
plt.xlabel('$x$', fontsize = 15)
plt.ylabel('$y$', fontsize = 15)
plt.title('Contour plot of $T(x, y)$')
plt.savefig(figure_folder + 'q1_Rohit_temp_contour.pdf')
plt.close()


#Error plot
T_analytical = _T_analytical(x)
error = np.abs(T_analytical - T)
index = np.arange(1, N + 1)
plt.plot(index, error / (10 ** -6), 'r-', linewidth = 2.0, marker = '^')
plt.title('Absolute Error ')
plt.xlabel('Node number')
plt.ylabel('$|T_{analytical} - T_{numerical}|$ x $10^{-6}$')
plt.savefig(figure_folder + 'q1_Rohit_error_plot.pdf')
plt.close()